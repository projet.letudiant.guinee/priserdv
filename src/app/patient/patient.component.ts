import { Component, OnInit } from '@angular/core';
import {Patient} from "../model/patient";
import {PatientHttpService} from "./patient-http.service";

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  patientForm: Patient = null;

  constructor(private patientService: PatientHttpService) {
  }


  ngOnInit(): void {
  }

  list(): Array<Patient> {
    return this.patientService.findAll();
  }

  add() {
    this.patientForm = new Patient();
  }

  edit(id: number) {
    this.patientService.find(id).subscribe(resp => {
      this.patientForm = resp;
    }, error => console.log(error));
  }

  save() {
    if (this.patientForm.id) {
      this.patientService.update(this.patientForm).subscribe(resp => {
        this.patientService.load();
      }, error => console.log(error));
    } else {
      this.patientService.create(this.patientForm).subscribe(resp => {
        this.patientService.load();
      }, error => console.log(error));
    }

    this.cancel();
  }

  remove(id: number) {
    this.patientService.remove(id);
  }

  cancel() {
    this.patientForm = null;
  }

}
