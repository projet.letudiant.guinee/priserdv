import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Patient} from "../model/patient";
@Injectable({
  providedIn: 'root'
})
export class PatientHttpService {

  patients: Array<Patient> = new Array<Patient>();

  constructor(private http: HttpClient) {
    this.load();
  }

  findAll(): Array<Patient> {
    return this.patients;
  }

  find(id: number): Observable<Patient> {
    return this.http.get<Patient>("http://localhost:3000/patients/" + id);
  }

  create(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>("http://localhost:3000/patients", patient);
  }

  update(patient: Patient): Observable<Patient> {
    return this.http.put<Patient>("http://localhost:3000/patients/" + patient.id, patient);
  }

  remove(id: number) {
    this.http.delete("http://localhost:3000/patients/" + id).subscribe(resp => {
      this.load();
    }, error => console.log(error));
  }

  load() {
    this.http.get<Array<Patient>>("http://localhost:3000/patients").subscribe(response => {
      this.patients = response;
    }, error => console.log(error));
  }

}
