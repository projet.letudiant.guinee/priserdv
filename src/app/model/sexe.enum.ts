export enum Sexe {
  HOMME = 'HOMME',
  FEMME = 'FEMME',
  INDEFINI = 'INDEFINI'
}
