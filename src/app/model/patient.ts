import {Adresse} from "./adresse";
import {Sexe} from "./sexe.enum";
import {RendezVous} from "./rendezvous";

export class Patient {

  id: number;
  version: number;
  numeroSS: string;
  nom: string;
  prenom: string;
  email: string;
  telephone: string;
  adresse: Adresse;
  sexe: Sexe;
  rendezVous: RendezVous;

}
