import {Adresse} from "./adresse";
import {RendezVous} from "./rendezvous";

export class Praticien {

  id : number;
  version: number;
  nom : string;
  prenom : string;
  numActivite : number;
  specialitePrincipale: string;
  specialiteSecondaire: string;
  adresse = Adresse;
  rendezVous = RendezVous;
}
