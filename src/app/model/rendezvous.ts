import {Statut} from "./statut.enum";

export class RendezVous {
  id: number;
  version: number;
  // dtRendezVous: Date;
  motif:  string;
  conclusion:  string;
  statut = Statut;
}
